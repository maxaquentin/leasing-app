import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {Table,Collapse,Radio, Input,LocaleProvider, Modal, Menu, Select, Button}  from 'ant-design-vue'
import axios from 'axios'
import VueAxios  from 'vue-axios'
import 'ant-design-vue/dist/antd.css'

Vue.config.productionTip = false;

// Vue.prototype.$url = 'http://localhost'
Vue.prototype.$url = 'http://167.172.254.81'
Vue.use(VueAxios, axios)

Vue.use(Input)
Vue.use(Modal)
Vue.use(Menu)
Vue.use(Select)
Vue.use(LocaleProvider)
Vue.use(Button)
Vue.use(Collapse)
Vue.use(Radio)
Vue.use(Table)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')