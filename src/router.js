import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/clients',
      name: 'home',
      component: () => import('./views/Clients.vue'),
      meta: { 
      requiresAuth: true,
      }
    },
    {
      path: '/cars',
      name: 'as',
      component: () => import('./views/Cars.vue'),
      meta: { 
      requiresAuth: true,
      }
    },
    {
      path: '/contracts',
      name: 'asgasf',
      component: () => import('./views/Contracts.vue'),
      meta: { 
      requiresAuth: true,
      }
    },
    {
      path: '/transactions',
      name: 'asg',
      component: () => import('./views/Transactions.vue'),
      meta: { 
      requiresAuth: true,
      }
    },
    {
      path: '/table',
      name: 'table',
      component: () => import('./views/Table.vue'),
      meta: { 
      requiresAuth: true,
      }
    },
    {
      path: '/big-table',
      name: 'big-table',
      component: () => import('./views/BigTable.vue'),
      meta: { 
      requiresAuth: true,
      }
    },
    {
      path: '/contract/:id',
      name: 'contract',
      component: () => import('./views/Contract.vue'),
      meta: { 
      requiresAuth: true,
      }
    },
    {
      path: '/login',
      name: 'login',
       component: () => import('./views/Login.vue'),
    },
  ]
})


router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
      if (localStorage.getItem('auth') == null ){
          next({
              path: '/login',
              params: { nextUrl: to.fullPath }
          })
      }else{
          let user = JSON.parse(localStorage.getItem('user'))
          console.log(user)
          if(to.matched.some(record => record.meta.is_admin)) {
              
          }else {
              next()
          }
    }
  } else if(to.matched.some(record => record.meta.guest)) {
      if(localStorage.getItem('jwt') == null){
          next()
      }
      else{
          next({ name: 'sms-receive'})
      }
  }else {
      next() 
  }
})

export default router

