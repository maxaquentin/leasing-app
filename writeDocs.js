const connection   = require('./mongo_connect');
const readXlsxFile = require('read-excel-file/node');


readXlsxFile('./clients.xlsx').then(async (rows) => {
    rows.map(async (item, index) => {
        try {
            if(index == 0) return;
            let db         = await new connection;
            let dbase      = await db.db('leasing-app');
            let clients    = dbase.collection('clients');
            let cars       = dbase.collection('cars');
            let contracts  = dbase.collection('contracts');
            console.log(item)
            console.log(index)
            fname=item[15].split(' ')[2];
            if(item[15].split(' ')[3]) fname = fname+' '+item[15].split(' ')[3]
            let passport = [];
            if(item[17]) passport = item[17].split(' ');
            let newClient = await clients.insertOne({
                surname: item[15].split(' ')[0],
                name: item[15].split(' ')[1],
                fname: fname,
                passport: item[17],
                // ser:
                // num
                // whoissued
                // whenissued
                // address
                // real_address
                phone: item[15]
            });
            let car_form  = {
                mark: 'Шевроле',
                model: item[1],
                private: false,
                gay_cost:   Number(item[8]),
                not_cost:   Number(item[7]),
                other_cost: Number(item[9]),
                buy_price:  Number(item[6]),
                middle:     0,
                comission:  Number(item[11]),
                repair_cost:0,
                gps_cost:   Number(item[10]),
                total:      Number(item[12]),
                region_code: '01',
                num:        null,
                ser:        null,
                year:       item[3],
                tech_num:   item[5],
                owner:      item[26],
                purchase_day: item[4],
                status: 1
            }

            let exp = /[a-zA-Z]/g;
            if(item[1].substring(0,3).match(exp)) car_form.private = true;
            if(car_form.private){
                car_form.num = item[2].substring(3,5);
                car_form.ser1 = item[2][2];
                car_form.ser2 = item[2][6]+item[2][7]
            } else {
                car_form.num = item[2].substring(3,5);
                car_form.ser2 = item[2][5]+item[2][6]+item[2][7];
            }
            let newCar = await cars.insertOne({
                ...car_form
            });
            let contract_form = {
                client_name: item[15],
                client: newClient.insertedId,
                car_name: item[1],
                car_num: car_form.region_code+' ', 
                car: newCar.insertedId,
                price: item[18],
                initial_sum: item[19],
                coef: item[21],
                period: item[24],
                start_day: item[27],
                file: 0,
                sum: item[22],
                remain: item[20],
                pay: Number(item[25]),
                balance: item[33]
            };
            if(car_form.private) {
                contract_form.car_num += car_form.ser1+' '+car_form.num+' '+car_form.ser1
            } else {
                contract_form.car_num += car_form.num+' '+car_form.ser;
            }
            let newContract = await contracts.insertOne({
                ...contract_form
            })
            console.log(newContract.insertedId);
        } catch(err){
            console.log(index+1)
            throw err;
        }
    })
})