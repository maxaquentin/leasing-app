const express    = require('express');
let   router     = express.Router();
const bodyParser = require('body-parser');
const ObjectID   = require('mongodb').ObjectID;
const connection = require('./mongo_connect');
//  1 Запрос списка тарифов (get_tariffs_list)
var db = null;
var dbase = null;
var clients = null;
var deleted_clients = null;
var cars = null;
var deleted_cars = null;
var deleted_contracts = null;
var deleted_transactions = null;
var transactions = null;
var contracts = null;
var models = null;
var marks = null;
var mb = null;

router.use(bodyParser.json({ type: 'application/*+json' }))
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));


const allowCrossDomain = function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
}
router.use(allowCrossDomain)

router.use(async (req, res, next) => {
    db              = await new connection;
    dbase           = await db.db('leasing-app');
    clients         = await dbase.collection('clients');
    deleted_clients = await dbase.collection('deleted_clients');
    deleted_contracts = await dbase.collection('deleted_contracts');
    deleted_transactions = await dbase.collection('deleted_transactions');
    cars            = await dbase.collection('cars');
    deleted_cars    = await dbase.collection('deleted_cars');
    mb              = await dbase.collection('monthly_balance');
    contracts       = await dbase.collection('contracts');
    transactions    = await dbase.collection('transactions');
    models    = await dbase.collection('models');
    marks    = await dbase.collection('marks');
    next();
});

router.post('/login', async(req, res) => {
    try { 
      console.log(req.body)
      if(req.body.login == 'admin' && req.body.passw == 'fui89qwdj021iwo'){
        let user = {admin: 1, root:true, name:'Admin'}
        let token = 'fui89qwdj021iwoejcsijw9vub89UBF93R2U9FWN'
        res.send({user: token, user: user})
      }
      if(req.body.login == 'manager' && req.body.passw == 'as23sfoin72'){
        let user = {admin: 0, root:true, name:'Manager'}
        let token = 'asfasfsafs7237932anfkjfkjsabfkjsabfkjsf'
        res.send({user: token, user: user})
      } else {
        res.status(403).send('Invalid credentials');
      }
    } catch(err) {
      throw err;
    }
  });

router.post('/post/model', async (req, res) => {
    try {
        let newModel = await models.insertOne({ 
            ...req.body
        });
        res.send(newModel);
    } catch(err) {
        throw err;
    }
})


router.post('/post/mark', async (req, res) => {
    try {
        let newModel = await marks.insertOne({ 
            ...req.body
        });
        res.send(newModel);
    } catch(err) {
        throw err;
    }
})

router.post('/edit/client/:id', async (req, res) => {
    try {
        delete req.body._id;
        let client = await clients.updateOne({ 
            _id: ObjectID(req.params.id)
        }, {
            $set: {
                ...req.body
            }
        });
        res.send({id: req.params.id});
    } catch(err) {
        throw err;
    }
});


router.post('/edit/contract/:id', async (req, res) => {
    try {
        delete req.body._id;
        await contracts.updateOne({ 
            _id: ObjectID(req.params.id)
        }, {
            $set: {
                ...req.body
            }
        });
        res.send({id: req.params.id});
    } catch(err) {
        throw err;
    }
});

router.post('/edit/car/:id', async (req, res) => {
    try {
        delete req.body._id;
        let car = await cars.updateOne({ 
            _id: ObjectID(req.params.id)
        }, {
            $set: {
                ...req.body
            }
        });
        res.send({id: req.params.id});
    } catch(err) {
        throw err;
    }
});

router.post('/delete/client/:id', async (req, res) => {
    try {
        let client = await clients.findOne({ 
            _id: ObjectID(req.params.id)
        });
        await deleted_clients.insertOne({
            ...client
        })
        let deleted = await clients.deleteOne({
            _id: ObjectID(req.params.id)
        })
        console.log(deleted)
        res.send({id: req.params.id});
    } catch(err) {
        throw err;
    }
});


router.post('/delete/contract/:id', async (req, res) => {
    try {
        let contract = await contracts.findOne({ 
            _id: ObjectID(req.params.id)
        });
        await deleted_contracts.insertOne({
            ...contract
        })
        let deleted = await contracts.deleteOne({
            _id: ObjectID(req.params.id)
        })
        console.log(deleted)
        res.send({id: req.params.id});
    } catch(err) {
        throw err;
    }
});


router.post('/delete/car/:id', async (req, res) => {
    try {
        let car = await cars.findOne({ 
            _id: ObjectID(req.params.id)
        });
        await deleted_cars.insertOne({
            ...car
        })
        await cars.deleteOne({
            _id: ObjectID(req.params.id)
        })
        res.send({id: req.params.id});
    } catch(err) {
        throw err;
    }
});

router.post('/post/car', async (req, res) => {
    try {
        let newCar = await cars.insertOne({ 
            ...req.body
        });
        res.send(newCar);
    } catch(err) {
        throw err;
    }
})

router.post('/post/client', async (req, res) => {
    try {
        let newClient = await clients.insertOne({ 
            ...req.body
        });
        res.send(newClient);
    } catch(err) {
        throw err;
    }
})

router.post('/post/transaction', async (req, res) => {
    try {
        var newtransaction = await transactions.insertOne({ 
            ...req.body
        });
        req.body.sum = parseInt(req.body.sum)
        req.body.pay = parseInt(req.body.pay)
        let ct = await contracts.findOne({_id: ObjectID(req.body.contract)})
        let real_balance = 0;

        if (ct.balance) real_balance = ct.balance 
        let b_update = await contracts.updateOne({_id: ObjectID(req.body.contract)}, {
            $set: {
                balance: req.body.sum+real_balance
            }
        })
        console.log(b_update)
        console.log(newtransaction.insertedId)
        var mbalance = await mb.find({
            year:new Date(req.body.date).getFullYear(),
            contract: req.body.contract
        }).toArray();
        var balance = 0
        var month = new Date().getMonth()+1
        if(mbalance.length) balance = mbalance[mbalance.length-1].sum
        if(mbalance.length) month = mbalance[mbalance.length-1].month
            if(parseInt(req.body.sum)+balance > req.body.pay) {
                await mb.updateOne({
                    month: month,
                    year:new Date(req.body.date).getFullYear(),
                    contract: req.body.contract
                }, {
                    $set: {
                        sum: req.body.pay,
                        last_time: new Date()
                    },
                    $addToSet: {
                        transactions: newtransaction.insertedId
                    }
                }, {upsert: true});

                let remain = req.body.sum - (req.body.pay - balance);
                let by = remain/(req.body.pay)
                if(by > 1){
                    let lilpart = (by - parseInt(by))*req.body.pay;
                    for(let i=1;i<parseInt(by)+1;i++){
                        await mb.updateOne({
                            month: month+i,
                            year:new Date(req.body.date).getFullYear(),
                            contract: req.body.contract
                        }, {
                            $set: {
                                sum: req.body.pay,
                                last_time: new Date()
                            },
                            $addToSet: {
                                transactions: newtransaction.insertedId
                            }
                        }, {upsert: true});
                    }
                    await mb.updateOne({
                        month: month+1+by,
                        year:new Date(req.body.date).getFullYear(),
                        contract: req.body.contract
                    }, {
                        $set: {
                            sum: lilpart,
                            last_time: new Date()
                        },
                        $addToSet: {
                            transactions: newtransaction.insertedId
                        }
                    }, {upsert: true}); 
                } else {
                    let lilpart = (by - parseInt(by))*req.body.pay;
                    console.log(by)
                    await mb.updateOne({
                        month: month+1,
                        year:new Date(req.body.date).getFullYear(),
                        contract: req.body.contract
                    }, {
                        $set: {
                            sum: lilpart,
                            last_time: new Date()
                        },
                        $addToSet: {
                            transactions: newtransaction.insertedId
                        }
                    }, {upsert: true}); 
                }
            } else {
                await mb.updateOne({
                    month: month,
                    year:new Date(req.body.date).getFullYear(),
                    contract: req.body.contract
                }, {
                    $set: {
                        sum: parseInt(req.body.sum)+balance,
                        last_time: new Date()
                    },
                    $addToSet: {
                        transactions: newtransaction.insertedId
                    }
                }, {upsert: true});
            }
        res.send(newtransaction);
    } catch(err) {
        throw err;
    }
})

router.post('/post/contract', async (req, res) => {
    try {
        let newContract = await contracts.insertOne({ 
            ...req.body
        });
        res.send(newContract);
    } catch(err) {
        throw err;
    }
})


router.get('/get/clients', async (req, res) => {
    try {
        let result = await clients.find({}).toArray(); 
        res.send(result);
    } catch (err){
        throw err;
    }
});


router.get('/get/marks', async (req, res) => {
    try {
        let result = await marks.find({}).toArray(); 
        res.send(result);
    } catch (err){
        throw err;
    }
});


router.get('/get/models', async (req, res) => {
    try {
        let result = await models.find({}).toArray(); 
        res.send(result);
    } catch (err){
        throw err;
    }
});



router.get('/get/big-table', async (req, res) => {
    try {
        let idConversionStage = {
            $addFields: {
                client: { $toObjectId: "$_client" }
            }
         };
        let result = await contracts.aggregate([
            idConversionStage, 
            {
                "$project": {
                    "car": {
                      "$toObjectId": "$car"
                    },
                  "client_name":1,
                  car_name: 1,
                  car_num: 1,
                  price: 1,
                  balance: 1,
                  initial_sum: 1,
                  start_day: 1,
                  pay: 1,
                  file: 1,
                  coef: 1,
                  sum: 1,
                  remain: 1,
                  period: 1,
                }
            },
            {
              $lookup:{
                  from: "clients",
                  localField: "client",
                  foreignField: '_id',
                  as: "driver"
                }
           },
           {
            $lookup:
              {
                from: "cars",
                localField: "car",
                foreignField: "_id",
                as: "auto"
              }
        }
        ]).toArray();
        res.send(result);
    } catch (err){
        throw err;
    }
});


router.get('/get/contract/:id', async (req, res) => {
    try {
        let result =  await contracts.aggregate([
            {
                $match: {
                    _id: ObjectID(req.params.id)                    
                }
            },
            {
                "$project": {
                  "_id": {
                    "$toString": "$_id"
                  },
                  "car": {
                    "$toObjectId": "$car"
                  },
                  "client": {
                      "$toObjectId": "$client"
                    },
                client_name:1,
                car_name: 1,
                car_num: 1,
                price: 1,
                initial_sum: 1,
                start_day: 1,
                pay: 1,
                file: 1,
                coef: 1,
                sum: 1,
                remain: 1,
                period: 1,
                }
            },
            {
              $lookup:
                {
                  from: "transactions",
                  localField: "_id",
                  foreignField: "contract",
                  as: "trs"
                }
           },
           {
            $lookup:
              {
                from: "monthly_balance",
                localField: "_id",
                foreignField: "contract",
                as: "payments"
              }
        },
        {
            $lookup:
            {
                from: "cars",
                localField: "car",
                foreignField: "_id",
                as: "cars"
            }
        },
        {
            $lookup:
              {
                from: "clients",
                localField: "client",
                foreignField: "_id",
                as: "clients"
              }
        },
        ]).toArray();

        res.send(result);
    } catch (err){
        throw err;
    }
})

router.get('/get/table', async (req, res) => {
    try {
        let result = await contracts.aggregate([
            {
                "$project": {
                  "_id": {
                    "$toString": "$_id"
                  },
                "client_name":1,
                client: 1,
                car_name: 1,
                car_num: 1,
                car: 1,
                price: 1,
                initial_sum: 1,
                start_day: 1,
                pay: 1,
                file: 1,
                coef: 1,
                sum: 1,
                remain: 1,
                period: 1,
                }
            },
            {
              $lookup:
                {
                  from: "transactions",
                  localField: "_id",
                  foreignField: "contract",
                  as: "trs"
                }
           },
           {
            $lookup:
              {
                from: "monthly_balance",
                localField: "_id",
                foreignField: "contract",
                as: "payments"
              }
         }
        ]).toArray();
        res.send(result);
    } catch (err){
        throw err;
    }
});

router.get('/get/contracts', async (req, res) => {
    try {
        let result = await contracts.find({}).toArray(); 
        res.send(result);
    } catch (err){
        throw err;
    }
})

router.post('/delete/transaction/:id', async (req, res) => {
    try{
        console.log(req.body)
        delete req.body._id;
        await deleted_transactions.insertOne({...req.body}); 
        let ct = await contracts.findOne({_id: ObjectID(req.body.contract)})
        let real_balance = 0;

        if(ct) {
            if (ct.balance) real_balance = ct.balance 
            await contracts.updateOne({_id: ObjectID(req.body.contract)}, {
                $set: {
                    balance: real_balance-req.body.sum
                }
            })
        }
        let result = await transactions.deleteOne({_id: ObjectID(req.params.id)});
        res.send(result);
    } catch (err){
        throw err;
    }
});

router.get('/get/transactions', async (req, res) => {
    try {
        let result = await transactions.aggregate([
            { $lookup: {
                from: "contracts",
                localField: "_id",
                foreignField: "contract",
                as: 'contract_info'
            }}
        ]).toArray(); 
        res.send(result);
    } catch (err){
        throw err;
    }
});

router.get('/get/cars', async (req, res) => {
    try {
        let result = await cars.find({}).toArray(); 
        res.send(result);
    } catch (err){
        throw err;
    }
})

module.exports = router;